CONTENTS OF THIS FILE - 
-----------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Entity Reference Revision Add Edit Display Dialog

Allow Entity Reference Revision to be added/edited/viewed within
modal dialog, without requiring a page reload.


REQUIREMENTS
------------

Entity Browser


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.
